import './customEventPolyfill';

/**
 * Enables Task List update behavior.
 */
export default class TaskList {

  constructor(settings = {}) {
    if (!settings.markdownContainer || !settings.renderedContainer) {
      throw new Error('Must pass an object to `new TaskList()` with `markdownContainer` and `renderedContainer` properties.');
    }

    this.incompleteItem = '[ ]';
    this.completeItem = '[x]';

    this.incompletePattern = RegExp(this.escapePattern(this.incompleteItem));
    this.completePattern = RegExp(this.escapePattern(this.completeItem));

    /**
     * Used to skip checkbox markup inside of code fences.
     */
    this.startFencesPattern = /^`{3}.*$/;
    this.endFencesPattern = /^`{3}$/;
    this.itemsInParasPattern = RegExp(`^(${this.escapePattern(this.completeItem)}|${this.escapePattern(this.incompleteItem)}).+$`, 'g');
    this.itemInCodeFencesPattern = new RegExp(/```\n[\s\S]+?```/, 'g');
    this.itemPattern = RegExp(`^(?:\\s*[-+*]|(?:\\d+\\.))?\\s*(${this.escapePattern(this.completeItem)}|${this.escapePattern(this.incompleteItem)})(?=\\s)`);

    this.onUpdate = settings.onUpdate;

    this.markdownContainer = document.querySelector(settings.markdownContainer);
    this.renderedContainer = document.querySelector(settings.renderedContainer);

    this.renderedContainer.addEventListener('change', (event) => {
      this.update(event.target);
    });

    this.enable();
  }

  /**
   * Removes disabled attribute from checkboxes.
   * Emits event to warn about change.
   */
  enable() {
    const checkboxes = this.renderedContainer.querySelectorAll('.task-list-item-checkbox');

    Array.from(checkboxes).forEach((checkbox) => {
      checkbox.removeAttribute('disabled');
    });

    this.emitEvent('taskList:enabled');
  }

  /**
   * Adds disabled attribute to checkboxes.
   * Emits event to warn about change.
   */
  disable() {
    const checkboxes = this.renderedContainer.querySelectorAll('.task-list-item-checkbox');

    Array.from(checkboxes).forEach((checkbox) => {
      checkbox.setAttribute('disabled', true);
    });

    this.emitEvent('taskList:disabled');
  }

  /**
   * Updates the markdown container value to reflect
   * the change of the renderedContainer.
   * Emits `tasksList:change` event before the value has changed and
   * emits `tasksList:changed` event once the value has changed.
   *
   * @param  {type} item description
   * @return {type}      description
   */
  update(item) {
    const checkboxes = this.renderedContainer.querySelectorAll('.task-list-item-checkbox');
    const index = 1 + Array.from(checkboxes).indexOf(item);
    const isChecked = item.checked;
    const event = new CustomEvent('taskList:change', {
      detail: { index, isChecked }
    });

    document.dispatchEvent(event);

    if (event.defaultPrevented) {
      return;
    }

    const updatedMarkdown = this.updateItem(this.markdownContainer.value, index, isChecked);

    this.markdownContainer.value = updatedMarkdown;
    this.markdownContainer.dispatchEvent(new Event('change'));
    this.markdownContainer.dispatchEvent(new CustomEvent('taskList:changed', {
      detail: { index, isChecked }
    }));

    if (this.onUpdate && updatedMarkdown) {
      this.onUpdate(updatedMarkdown);
    }
  }

  /**
   * Given the provided source, updates the correct task list item according to
   * the given checked value.
   *
   * @param  {String} source
   * @param  {Number} itemIndex
   * @param  {Boolean} isChecked
   * @returns {String}
   */
  updateItem(source, itemIndex, isChecked) {
    const cleanSource = source
      .replace(/\r/g, '')
      .replace(this.itemsInParasPattern, '')
      .split('\n');

      // Removing the codefences would solve half the problem:
      // But it would not solve the problem if duplicated task lists item.
      // 
      // Adding the following would work on this scenario:
      // ```
      // - [ ] item 3
      // - [ ] item 4
      // ```
      // - [ ] item 1
      // - [ ] item 2
      // 
      // but would fail on this:
      // ```
      // - [ ] item 1
      // - [ ] item 2
      // ```
      // - [ ] item 1
      // - [ ] item 2
      // 
      // The problem is not in the codefences it self, but in the way we find the item.
      // 
      // .replace(this.itemInCodeFencesPattern, (match) => {
      //   const matched = match.split('\n');
      //   return matched.map(() => '\n').join('');
      // })

    let index = 0;
    let isLineInsideCodeBlock = false;

    return source.split('\n').map((line) => {
      // If this line is the end of a code block
      if (isLineInsideCodeBlock && this.endFencesPattern.test(line)) {
        isLineInsideCodeBlock = false;
      }

      // If this line is the start of a code block
      if (!isLineInsideCodeBlock && this.startFencesPattern.test(line)) {
        isLineInsideCodeBlock = true;
      }

      // If this line is not inside a code block, exists in the cleanSource Array
      // and it's a task list item
      if (!isLineInsideCodeBlock &&
        cleanSource.indexOf(line) >= 0 &&
        this.itemPattern.test(line)) {

        index += 1;

        if (itemIndex === index) {
          if (isChecked) {
            return line.replace(this.incompletePattern, this.completeItem);
          } else {
            return line.replace(this.completePattern, this.incompleteItem);
          }
        } else {
          return line;
        }
      }

      return line;

    }).join('\n');
  }

  destroy() {
    // TODO: FIX THIS
    this.renderedContainer.removeEventListener('change', this.update);
  }

  /**
   * Creates and dispatches an event with the type provided.
   *
   * @param  {String} eventType
   */
  emitEvent(eventType) {
    const event = new Event(eventType);
    document.dispatchEvent(event);
  }

  /**
   * Escapes the provided string.
   *
   * @param  {string} string
   * @returns {String}        The escaped string
   */
  escapePattern(string) {
    return string
      .replace(/([\[\]])/g, '\\$1') // escape square brackets
      .replace(/\s/, '\\s')         // match all whitespace
      .replace('x', '[xX]');        // match all casess
  }
}
