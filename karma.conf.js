const DefinePlugin = require('webpack/lib/DefinePlugin');

module.exports = function(config) {
  config.set({
    basePath: '',

    frameworks: ['mocha', 'sinon', 'sinon-chai'],

    files: [
      'node_modules/babel-polyfill/dist/polyfill.js',
      'tests/*.js'
    ],

    webpack: {
      module: {
        loaders: [
          { test: /\.js$/, exclude: /node_modules/, loader: 'babel' },
          { test: /\.s?css$/, loader: 'style!css' }
        ]
      },
      plugins: [
        new DefinePlugin({ GH_ACCESS_TOKEN: JSON.stringify(process.env.GH_ACCESS_TOKEN) })
      ]
    },
    webpackMiddleware: {
      noInfo: true
    },
    plugins: [
      "karma-webpack",
      "karma-mocha",
      "karma-mocha-reporter",
      "karma-phantomjs-launcher",
      "karma-chrome-launcher",
      "karma-firefox-launcher",
      "karma-coverage",
      "karma-sinon",
      "karma-sinon-chai"
    ],

    preprocessors: {
      'tests/*.js': ['webpack']
    },

    client: {
      mocha: {
        reporter: 'spec',
        ui: 'bdd'
      }
    },

    reporters: ['mocha', 'coverage'],

    port: 9876,

    colors: true,

    logLevel: config.LOG_INFO,

    autoWatch: false,

    browsers: ['Chrome', 'PhantomJS', 'Firefox'],

    singleRun: true,

    concurrency: Infinity,

    coverageReporter: {
      reporters: [
        {
          type: 'text-summary'
        }
      ],
      includeAllSources: true
    }
  });
}
