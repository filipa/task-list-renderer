## Task List
Turns a GFM task list into a list of interactive checkboxes.

> This plugin is based on the work done on [github-archive/task_list](https://github.com/github-archive/task_list) that has been deprecated.

> And on the work done on [gfm-task-list](https://github.com/waffleio/gfm-task-list/) but without jQuery as a dependency.

> Includes bug fixing from the work done on [deckar01/task_list](https://github.com/deckar01/task_list)

## Development

### Setup
You will need to install [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/) if you do not already have them.

To install the dependencies run:
```bash
npm install
```

### Environment Variables
In order to run the development mode you will need a valid GitHub access token. You can create one in the [tokens page](https://github.com/settings/tokens).

### Running development server

Start up your server by runnig:
```bash
GH_ACCESS_TOKEN=<your-github-token> npm run start
```
Navigate to [http://localhost:3000](http://localhost:3000) to see the development page.

## Tests

### Running tests
```bash
GH_ACCESS_TOKEN=<your-github-token> npm run test
```

## Eslint
```bash
npm run lint
```