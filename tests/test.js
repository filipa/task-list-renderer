/* eslint-disable */
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import * as request from 'superagent';
import TaskList from '../src/';

chai.use(sinonChai);

describe('TaskList', () => {
  const renderMarkdown = markdown => new Promise((resolve, reject) => {
    document.querySelector('.source').value = markdown.join('\n');

    request
      .post('https://api.github.com/markdown')
      .set('Authorization', `Bearer ${GH_ACCESS_TOKEN}`)
      .set('Content-Type', 'application/json')
      .send({
        mode: 'gfm',
        text: markdown.join('\n')
      })
      .end((err, res) => {
        if (err) {
          return reject(err);
        }
        document.querySelector('.output').innerHTML = res.text;
        return resolve();
      });
  });

  beforeEach('setup the needed html structure', () => {
    const container = document.createElement('div');
    container.className = 'container';
    const textarea = document.createElement('textarea');
    textarea.className = 'source';
    const output = document.createElement('div');
    output.className = 'output';

    container.appendChild(textarea);
    container.appendChild(output);

    document.body.appendChild(container);
  });

  afterEach('clean the html', () => {
    document.body.removeChild(document.querySelector('.container'));
  });

  describe('Initalize', () => {
    it('should enable all the task list items when initialized', () => {
      const markdown = [
        '- [ ] item 1 ',
        '- [ ] item 2'
      ];

      return renderMarkdown(markdown).then(() => {
        new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output'
        });

        const checkboxes = document.querySelectorAll('.task-list-item-checkbox');
        Array.from(checkboxes).forEach((checkbox) => {
          return expect(checkbox.getAttribute('disabled')).to.equal(null);
        });
      });
    });

    it('should throw an error if required props are not provided', () => {
      const fn = () => new TaskList();

      expect(fn).to.throw('Must pass an object to `new TaskList()` with `markdownContainer` and `renderedContainer` properties.');
    });

    it('should not have any sidefect on lists with no tasks', () => {
      const markdown = ['_italic_'];

      return renderMarkdown(markdown).then(() => {
        new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output'
        });

        expect(document.querySelector('.output').textContent).to.equal('italic');
      });
    });
  });

  describe('Disable', () => {
    it('should disable all the task list items', () => {
      const markdown = [
        '- [ ] item 1',
        '- [ ] item 2'
      ];

      return renderMarkdown(markdown).then(() => {
        const taskList = new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output'
        });
        taskList.disable();

        const checkboxes = document.querySelectorAll('.task-list-item-checkbox');

        Array.from(checkboxes).forEach((checkbox) => {
          expect(checkbox.getAttribute('disabled')).to.equal('true');
        });
      });
    });
  });

  describe('Enable', () => {
    it('should enable all the task list items', () => {
      const markdown = [
        '- [ ] item 1',
        '- [ ] item 2'
      ];

      return renderMarkdown(markdown).then(() => {
        const taskList = new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output'
        });
        taskList.disable();
        taskList.enable();

        const checkboxes = document.querySelectorAll('.task-list-item-checkbox');

        Array.from(checkboxes).forEach((checkbox) => {
          expect(checkbox.getAttribute('disabled')).to.equal(null);
        });
      });
    });
  });

  describe.only('Update', () => {
    it('should update markdown when rendered checkbox is checked', () => {
      const callback = sinon.spy();

      const markdown = [
        '- [ ] item 1',
        '- [ ] item 2'
      ];

      const expectedMarkdown = [
        '- [x] item 1',
        '- [ ] item 2'
      ].join('\n');

      return renderMarkdown(markdown).then(() => {
        new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output',
          onUpdate: (updated) => callback(updated)
        });

        const checkbox = document.querySelector('.task-list-item-checkbox');
        checkbox.click();

        expect(callback).to.have.callCount(1);
        expect(callback).to.have.been.calledWith(expectedMarkdown);
      });
    });

    it('should update markdown when rendered checkbox is un-checked', () => {
      const callback = sinon.spy();

      const markdown = [
        '- [x] item 1',
        '- [ ] item 2'
      ];

      const expectedMarkdown = [
        '- [ ] item 1',
        '- [ ] item 2'
      ].join('\n');

      return renderMarkdown(markdown).then(() => {
        new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output',
          onUpdate: (updated) => callback(updated)
        });

        const checkbox = document.querySelector('.task-list-item-checkbox');
        checkbox.click();

        expect(callback).to.have.callCount(1);
        expect(callback).to.have.been.calledWith(expectedMarkdown);
      });
    });

    it('should not update task list item inside codefences', () => {
      const callback = sinon.spy();

      const markdown = [
        '```',
        '- [ ] item 1',
        '- [ ] item 2',
        '```',
        '- [ ] item 1',
        '- [ ] item 2'
      ];

      const expectedMarkdown = [
        '```',
        '- [ ] item 1',
        '- [ ] item 2',
        '``` ',
        '- [x] item 1',
        '- [ ] item 2'
      ].join('\n');

      return renderMarkdown(markdown).then(() => {
        new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output',
          onUpdate: (updated) => callback(updated)
        });

        const checkbox = document.querySelector('.task-list-item-checkbox');
        checkbox.click();

        expect(callback).to.have.callCount(1);
        expect(callback).to.have.been.calledWith(expectedMarkdown);
      });
    });

    it('should not update task list item inside single codefences', () => {
      const callback = sinon.spy();

      const markdown = [
        '`- [ ] item 1`',
        '- [ ] item 1',
        '- [ ] item 2'
      ];

      const expectedMarkdown = [
        '`- [ ] item 1`',
        '- [x] item 1',
        '- [ ] item 2'
      ].join('\n');

      return renderMarkdown(markdown).then(() => {
        new TaskList({
          markdownContainer: '.source',
          renderedContainer: '.output',
          onUpdate: (updated) => callback(updated)
        });

        const checkbox = document.querySelector('.task-list-item-checkbox');
        checkbox.click();

        expect(callback).to.have.callCount(1);
        expect(callback).to.have.been.calledWith(expectedMarkdown);
      });
    });

    describe('should update the correct checkbox when the item is duplicated in the same list', () => {
      let markdown;

      beforeEach(() => {
        markdown = [
          '- [ ] item 1',
          '- [ ] item 1'
        ];
      });

      it('and the first one is clicked', () => {
        const callback = sinon.spy();

        const expectedMarkdown = [
          '- [x] item 1',
          '- [ ] item 1'
        ].join('\n');

        return renderMarkdown(markdown).then(() => {
          new TaskList({
            markdownContainer: '.source',
            renderedContainer: '.output',
            onUpdate: (updated) => callback(updated)
          });

          const checkbox = document.querySelector('.task-list-item-checkbox');
          checkbox.click();

          expect(callback).to.have.callCount(1);
          expect(callback).to.have.been.calledWith(expectedMarkdown);
        });
      });

      it(' and the second one is clicked', () => {
        const callback = sinon.spy();
        const expectedMarkdown = [
          '- [ ] item 1',
          '- [x] item 1'
        ].join('\n');

        return renderMarkdown(markdown).then(() => {
          new TaskList({
            markdownContainer: '.source',
            renderedContainer: '.output',
            onUpdate: (updated) => callback(updated)
          });

          const checkbox = Array.from(document.querySelectorAll('.task-list-item-checkbox'))[1];
          checkbox.click();

          expect(callback).to.have.callCount(1);
          expect(callback).to.have.been.calledWith(expectedMarkdown);
        });
      });
    });

    describe('should update the correct checkbox when the item is duplicated in two different lists', () => {
      let markdown;

      beforeEach(() => {
        markdown = [
          '- [ ] item 1',
          '- [ ] item 1',
          ' ',
          'Another List:',
          '- [ ] item 1'
        ];
      });

      it('and the first one of the first list is clicked', () => {
        const callback = sinon.spy();

        const expectedMarkdown = [
          '- [x] item 1',
          '- [ ] item 1',
          ' ',
          'Another List:',
          '- [ ] item 1'
        ].join('\n');

        return renderMarkdown(markdown).then(() => {
          new TaskList({
            markdownContainer: '.source',
            renderedContainer: '.output',
            onUpdate: (updated) => callback(updated)
          });

          const checkbox = document.querySelector('.task-list-item-checkbox');
          checkbox.click();

          expect(callback).to.have.callCount(1);
          expect(callback).to.have.been.calledWith(expectedMarkdown);
        });
      });

      it('and the second one of the first list is clicked', () => {
        const callback = sinon.spy();
        const expectedMarkdown = [
          '- [ ] item 1',
          '- [x] item 1',
          ' ',
          'Another List:',
          '- [ ] item 1'
        ].join('\n');

        return renderMarkdown(markdown).then(() => {
          new TaskList({
            markdownContainer: '.source',
            renderedContainer: '.output',
            onUpdate: (updated) => callback(updated)
          });

          const checkbox = Array.from(document.querySelectorAll('.task-list-item-checkbox'))[1];
          checkbox.click();

          expect(callback).to.have.callCount(1);
          expect(callback).to.have.been.calledWith(expectedMarkdown);
        });
      });

      it('and the first one of the second list is clicked', () => {
        const callback = sinon.spy();
        const expectedMarkdown = [
          '- [ ] item 1',
          '- [ ] item 1',
          ' ',
          'Another List:',
          '- [x] item 1'
        ].join('\n');

        return renderMarkdown(markdown).then(() => {
          new TaskList({
            markdownContainer: '.source',
            renderedContainer: '.output',
            onUpdate: (updated) => callback(updated)
          });

          const checkbox = Array.from(document.querySelectorAll('.task-list-item-checkbox'))[2];
          checkbox.click();

          expect(callback).to.have.callCount(1);
          expect(callback).to.have.been.calledWith(expectedMarkdown);
        });
      });
    });

    it('should update the correct checkbox when value is nested', () => {

    });

    it('should emit "tasklist:change" event when checkbox is clicked', () => {

    });

    it('should emit "tasklist:changed" event when the markdown is updated', () => {

    });

    it('should not emit "tasklist:changed" event when event has "preventDefault" called on it', () => {

    });

    it('should be case insensitive', () => {

    });

    it('should call the provided callback', () => {

    });
  });
});
