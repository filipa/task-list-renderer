const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const DefinePlugin = require('webpack/lib/DefinePlugin');

module.exports = {
  entry: [
    'webpack/hot/only-dev-server',
    'babel-polyfill',
    './dev/index.js'
  ],
  output: {
    path: './dist/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel' },
      { test: /\.s?css$/, loader: 'style!css' }
    ]
  },
  plugins: [
    new DefinePlugin({ GH_ACCESS_TOKEN: JSON.stringify(process.env.GH_ACCESS_TOKEN) }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: './dev/index.html'
    })
  ],
  devServer: {hot: true, colors: true}
}