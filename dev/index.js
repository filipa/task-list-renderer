import * as request from 'superagent';
import TaskList from '../src';

document.querySelector('.render-btn').addEventListener('click', () => {
  const markdown = document.querySelector('.source').value;

  if (markdown.length) {
    request
      .post('https://api.github.com/markdown')
      .set('Authorization', `Bearer ${GH_ACCESS_TOKEN}`)
      .set('Content-Type', 'application/json')
      .send({
        mode: 'gfm',
        text: markdown
      })
      .end((err, res) => {
        document.querySelector('.output').innerHTML = res.text;

        new TaskList({
          renderedContainer: '.output',
          markdownContainer: '.source',
          onUpdate: (updated) => {
            console.log(updated);
          }
        });
      });
  }
});
